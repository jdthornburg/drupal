<?php

function simple_flexslider_form($form, &$form_state, $opts = NULL){

    $views = simple_flexslider_get_views();
    $options = simple_flexslider_get_options();
    $markup = simple_flexslider_existing_blocks();
    
    if($opts){
        $record = simple_flexslider_get_existing_options($opts);
    }
                
    $form['name'] = array(
        '#title' => "Title of block",
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => empty($record['name']) ? "" : $record['name'],
        '#description' => empty($record['name']) ? "" : "If the name is changed, you will have to to reassign the block",
    );
        
    $form['view'] = array(
        '#title' => "View to use",
        '#type' => 'select',
        '#required' => TRUE,
        '#description' => '*NOTE: this module will use the default view options for the block that is created',
        '#options' => $views,
        '#default_value' => empty($record['view']) ? null : $record['view'],
    );

    $form['options'] = array(
        '#title' => "Options properties for this slideshow",
        '#type' => 'checkboxes',
        '#description' => 'Flexslider options. Please visit <a href="http://www.woothemes.com/flexslider/">http://www.woothemes.com/flexslider/</a> for documentation',
        '#options' => $options,
        '#default_value' => empty($record['options']) ? null : array_keys(simple_flexslider_get_default_options($record['options'])),
    );
    
    if(!empty($record)){
        $form['existing_record'] = array(
            '#type' => 'hidden',
            '#value' => $opts,
        );
    }
        
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => empty($record) ? 'Create Block' : 'Save Block'
    );
    
    return $form;
    
}

function simple_flexslider_get_views(){
    
    $options = array();
    $views = views_get_all_views();
        
    foreach($views as $key => $view){
        $options[$key] = $view->human_name . " [" . $key . "]";
    }
        
    return $options;
}

function simple_flexslider_get_options(){
    $options = array(
        'slideshow' => 'Start the slideshow automatically (slideshow)',
        'animateionLoop' => 'Repeat slideshow at end (animationLoop)',
        'smoothHeight' => 'Adjust the height of each slide as the slideshow progresses (smoothHeight)',
        'randomize' => 'Randomize the order (randomize)',
        'pauseOnHover' => 'Pause the slideshow when a user hovers over it (pauseOnHover)',
        'pauseOnAction' => 'Pause the slideshow when a user interacts with it (pauseOnAction)',
        'controlNav' => 'Display a key for the slideshow (controlNav)',
        'directionNav' => 'Display directional navigation on the slideshow (directionNav)',
    );
    
    return $options;
}

function simple_flexslider_form_validate($form, &$form_state){
    $options = array(
        'slideshow' => 'Start the slideshow automatically (slideshow)',
        'animateionLoop' => 'Repeat slideshow at end (animationLoop)',
        'smoothHeight' => 'Adjust the height of each slide as the slideshow progresses (smoothHeight)',
        'randomize' => 'Randomize the order (randomize)',
        'pauseOnHover' => 'Pause the slideshow when a user hovers over it (pauseOnHover)',
        'pauseOnAction' => 'Pause the slideshow when a user interacts with it (pauseOnAction)',
        'controlNav' => 'Display a key for the slideshow (controlNav)',
        'directionNav' => 'Display directional navigation on the slideshow (directionNav)',
    );
    
    return $options;    
}

function simple_flexslider_form_submit($form, &$form_state){
    $values = $form_state['values'];
    $options = array();
            
    foreach($values['options'] as $key => $value){
        if(is_int($value)){
            $options[$key] = "false";
        } else {
            $options[$key] = "true";
        }
    }
    
    $options = serialize($options);
    
    $data = array(
        'name' => $values['name'],
        'view' => $values['view'],
        'options' => $options,
    );
        
    if($values['existing_record']){
        $data['id'] = $values['existing_record'];
        drupal_write_record('simple_flexslider',$data,'id');
    } else {
        drupal_write_record('simple_flexslider',$data);
    }
        

    
}


function simple_flexslider_existing_blocks(){ 

    $query = "SELECT name, id, view, options FROM simple_flexslider";

    $results = db_query($query);
    
    $rows = array();
    
    foreach($results as $result){
        $row = array();
        $row['name'] = $result->name;
        $row['view'] = $result->view;
        $row['options'] = simple_flexslider_process_options($result->options);
        $row['edit'] = l('[edit]','admin/config/simple-flexslider/'.$result->id.'/edit');
        $row['delete'] = l('[delete]','simple-flexslider/'.$result->id.'/delete');
        $rows[] = $row;
    }
    
    $header = array(
        'Name' => array(
            'data' => 'Name',
            'field' => 'name'
        ),
        'View' => array(
            'data' => 'View',
            'field' => 'view'
        ),
        'Options' => array(
            'data' => 'Options',
        ),
        'Edit' => array(
            'data' => 'Edit',
        ),
        'Delete' => array(
            'data' => 'Delete'
        ),
    );
    
    $empty = "There are no slideshow blocks defined yet.";
    
    $table = l('Add Block','admin/config/simple_flexslider/add');
    $table .= "<h2>Existing Blocks</h2>";
    $table .= theme_table(
        array(
            'header' => $header,
            'rows' => $rows, 
            'attributes' => array(), 
            'caption' => NULL, 
            'colgroups' => NULL, 
            'sticky' => NULL,
            'empty' => $empty
        )
    );
            
    return $table;    
}

function simple_flexslider_get_existing_options($id){
    $query = "SELECT name, view, options FROM {simple_flexslider} WHERE id = :id";
    $result = db_query($query, array(':id'=>$id))->fetchObject();  
    
    $values = array(
        'name' => $result->name,
        'view' => $result->view,
        'options' => unserialize($result->options),
    );
    
    return $values;   
}

function simple_flexslider_get_default_options($options){
    $opts = array();
    foreach($options as $key => $value){
        if($value === "true"){
            $opts[$key] = $value;
        }
    }
    
    return $opts;
}

function simple_flexslider_process_options($opts){
    $options = unserialize($opts);
    
    $string = array();
    
    foreach($options as $key => $opt){
        if($opt == "true"){
            $string[] .= $key;
        }
    }
        
    return implode($string, ", ");
}

?>