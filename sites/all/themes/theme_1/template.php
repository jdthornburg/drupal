<?php

/** Theme stuff*/

//If the right column region is populated, add a class - 'right-callout' - to the classes array

	function theme1_preprocess_html(&$variables)
	{
		if (!empty($variables['page']['right_callout']))
		{
			$variables['classes_array'][] = "right-callout";
		}
	}
	
	function theme_1_preprocess_page(&$variables)
	{
		$widthClasses = array();
		
		if (!empty($variables['page']['left_callout']) && !empty($variables['page']['right_callout']))
		{
			$widthClasses['main'] = "one-half";
			$widthClasses['column'] = "one-fourth";
		}
		
		elseif (!empty($variables['page']['left_callout']) || !empty($variables['page']['right_callout']))
		{
			$widthClasses['main'] = "three-fourths";
			$widthClasses['column'] = "one-fourth";
		}
		
		else
		{
			$widthClasses['main'] = "";
		}
	
		$variables['widthClasses'] = $widthClasses;
		
		//dpm($variables);
	}	
		
	
	
?>